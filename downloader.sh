#!/bin/bash

# Libre Media Collection: Project to provide free/libre software to be
# distributed in removable media.
# Copyright (C) 2016, 2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>
#
# This file is part of Libre Media Collection.
#
# Libre Media Collection is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Libre Media Collection is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Libre Media Collection.  If not, see <https://www.gnu.org/licenses/>.

shopt -s nocasematch

do_exit() {
  exit $?
}

trap do_exit ERR

. "/etc/os-release"

download_packages() {
  if [[ "$ID" == "debian" || \
    "$ID_LIKE" =~ (^|[[:space:]]+)debian($|[[:space:]]+) ]]; then

    apt-get download $1
    mkdir "src" || true
    pushd "src"
    apt-get -d source $1
    popd
  fi
}

while IFS="," read prefix url_or_package_names base_name; do
  echo "Prefix:" ${prefix:?}
  mkdir -p "$prefix"
  pushd "$prefix"
  if [[ "$url_or_package_names" =~ ^(https?|ftp):// ]]; then
    wget ${base_name:+-O} "$base_name" "$url_or_package_names"
  else
    download_packages "$url_or_package_names"
  fi
  popd
done < "$1"
